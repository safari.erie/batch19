var nama1 = "";
var peran1 = "";


console.log("==================");
console.log("Jawaban Condition If");

if(nama1 == "" && peran1 == ""){
    console.log("Nama harus disi")
}

nama1 = "Eri";
if(nama1 == 'Eri' && peran1 == ""){
    console.log("Hallo "+nama1+", Pilih peranmu untuk memulai game!");
}else{
    console.log("tidak terdifinisi");
}

nama1 = "Jane";
peran1 = "Penyihir";
nama1 = "Jenita";
peran1 = "Guard";
if(nama1 == "Jane" && peran1 == "Penyihir"){
    console.log("Helo "+peran1+" "+nama1+" ,kamu dapat melihat siapa yang menjadi werewolf!");
}else{
    console.log("tidak terdefinisi");
}

nama1 = "Jenita";
peran1 = "Guard";
if(nama1 == "Jenita" && peran1 == "Guard"){
    console.log("Selamat Datang di Dunia Werewolf, "+nama1);
    console.log("Hello "+peran1+" kamu akan membantu melindungi temanmu dari serangan werewolf.");
}


nama1 = "Junaedi";
peran1 = "Werewolf";
if(nama1 == "Junaedi" && peran1 == "Werewolf"){
    console.log("Selamat Datang di Dunia Werewolf, "+nama1);
    console.log("Hello "+peran1+" kamu akan memakan mangsa setiap malam");
}



console.log("==================");
console.log("Jawaban Condition Switch Case");
var hari = 21; 
var bulan = 01; 
var tahun = 1945;

switch(bulan){
    case 1 :{console.log(hari+" Januari "+tahun);break;}
    case 2 :{console.log(hari+" Februari "+tahun);break;}
    case 3 :{console.log(hari+" Maret "+tahun);break;}
    case 4 :{console.log(hari+" April "+tahun);break;}
    case 5 :{console.log(hari+" Mei "+tahun);break;}
    case 6 :{console.log(hari+" Juni "+tahun);break;}
    case 7 :{console.log(hari+" Juli "+tahun);break;}
    case 8 :{console.log(hari+" Agustus "+tahun);break;}
    case 9 :{console.log(hari+" September "+tahun);break;}
    case 10 :{console.log(hari+" Oktober "+tahun);break;}
    case 11 :{console.log(hari+" November "+tahun);break;}
    case 12 :{console.log(hari+" Desember "+tahun);break;}
    default:{console.log("inputan tidak sesuai")}
}