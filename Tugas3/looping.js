console.log("Tugas Looping");
console.log("================");
console.log("LOOPING PERTAMA (WHILE)");
console.log("Jawaban No 1")
let startLoop = 1;
let endLoop = 20;

while (startLoop <= endLoop) {
    if (startLoop % 2 ==0){
        console.log(startLoop+" - I Love coding");
    }
    startLoop++;
}

console.log("LOOPING KEDUA (WHILE)");
while (endLoop){
    if (endLoop % 2 == 0){
        console.log(endLoop+" - I will become a mobile developer");
    }
    endLoop--;
}


console.log("============================");
console.log("Jawaban No 2 Loop Menggunakan For");  
for (let i = 1; i <= 20; i++) {   
    if(i % 2 == 1 ){       
        if(i % 3 == 0){
            console.log(i + "- I Love Coding");
        }else{
            console.log(i + "- Santai");
        }
    }else{
        console.log(i + "- Berkualitas");
    }
}


console.log("=================================");
console.log("Jawaban NO 3 Membuat Tangga");
let maxOfLoop = 6;
let minOfLoop = 1;

for (let rows = maxOfLoop; rows >= minOfLoop; rows--) {
    let result = "";
    for (let col= maxOfLoop; col >= minOfLoop; col--) {
        result += "#"
    }
    console.log(result);
}

console.log("=================================");
console.log("Jawaban NO 4 Membuat Tangga");

let limitLoop = 7;
for (var row = 1; row <= limitLoop; row++) {
	var result = '';
	for (var j = limitLoop; j >= row; j--) {
		result += ("#");
	}
	console.log(result);
}


console.log("=================================");
console.log("Jawaban NO 5 Membuat Papan Catur");
let results = "";
for (let rows = 0; rows <= 8; rows++) {
   // cek kondisi ganjil n genap
  
   if(rows % 2 == 0 ){
        for (let cols = 0; cols < 8; cols++) {
            if(cols % 2 == 0){
                results += ' ';
            }else{
                results += '#';
            }
        }
        results += '\n'; // each finished append enter
   }else{
    for (let cols = 0; cols < 8; cols++) {
        if(cols % 2 == 0){
            results += '#';
        }else{
            results += ' ';
        }
    }
    results += '\n'; // each finished append enter
   }
}
console.log(results);


function myApp(){
	var total = 5
	var output = "";
    for (let index = 1; index <= total; index++) {
        for (let x = 1; x <= index; x++){
            output += j +"";
        }
        console.log(output);
        output = ""
    }
}
myApp();