
function teriak(){
    return "Hallo Sanbers";
}
console.log("================");
console.log("Tugas Function");
console.log("================");
console.log("Jawaban No 1")
console.log(teriak());

const num1 = 12;
const num2 = 4;

function kalikan(num1, num2) {
    return num1 * num2;
}

let hasilKali = kalikan(num1,num2);
console.log("Jawaban No 2 menggunakan function biasa :");
console.log(hasilKali);


let perkalianArrowFunction = (num1, num2) =>{
    return num1 * num2;
}
console.log("Jawaban No 2 menggunakan Arrow function")
console.log(perkalianArrowFunction(num1,num2));


let name = "Eri";
let age = 31;
let address = "Jln. Malioboro, Yogyakarta";
let hobby = "Game";

function introduce(name, age, address, hobby) {
    return "Nama Saya "+name+" umur saya "+age+" tahun, alamat saya di "+address+" dan saya punya hobby "+hobby;
}

console.log(introduce(name, age, address, hobby));