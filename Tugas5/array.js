
console.log("Tugas 5 Array  ");
console.log("================================");

let tempArrays = [];

function range(startNum, finishNum){
    if(startNum == null){
        return -1 +" => apabila start num tidak diisi ";
    }else if(finishNum == null){
        return -1 +" => apabila finis Num tidak diisi";
    }else if(startNum == null && finishNum == null){
        return -1 + " => apabila startnum dan finis num tidak diisi";
    }else{
        if(startNum > finishNum){
            for(var i = startNum; i >= finishNum; i--){
                tempArrays.push(i);
            }
        }else{
            for(var i = startNum; i <= finishNum; i++){
                tempArrays.push(i);
            }
        }
        
       return tempArrays;
        
    }
}

console.log("Jawaban Soal 1 Range Of Array ");
console.log(range(54,50));

function rangeWithStep(startNum, finishNum, step){
    if(startNum == null){
        return -1 +" => apabila start num tidak diisi ";
    }else if(finishNum == null){
        return -1 +" => apabila finis Num tidak diisi";
    }else if(startNum == null && finishNum == null){
        return -1 + " => apabila startnum dan finis num tidak diisi";
    }else{
        if(startNum > finishNum){
            for (var i = startNum; i > finishNum; i -= step) {
                tempArrays.push(i);          
             }
        }else{
            for (var i = startNum; i < finishNum; i += step) {
               tempArrays.push(i);
         
            }
        }        
       return tempArrays;
        
    }
}
console.log("================================")
console.log("Jawaban Soal 2 selisih Of Array ");

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log("================================")
console.log("Jawaban Soal 3 Sum ");
let sums = 0;
function sum(startNum, finishNum, step){
    if(startNum == null){
        return 0;
    }else if(finishNum == null){
        return 0;
    }else if(startNum == null && finishNum == null){
        return 0;
    }else{
       
        if(startNum > finishNum){
            if(step == null){
                var ret = 0;
                for (var i = startNum; i >= finishNum; i--) {
                    ret += i;
            
                }
                return ret;
            }else{
                var ret = 0;
                for (var i = startNum; i >= finishNum; i -= step) {
                    ret += i;
            
                }
                return ret;
            }
           
        }else{
            if(step == null){
                ret = 0;
                for (var i = startNum; i <= finishNum; i++) {
                    
                    ret +=i;
                }
                
                return ret;
            }else{
                ret = 0
                for (var i = startNum; i <= finishNum; i += step) {
                    ret += i;
            
                }
                return ret;
            }
            
        }        
        
    }
}


console.log(sum(1,10)) 
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1 */


function dataHandling(){
    
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ];

    for(var i = 0; i < input.length; i++){
       /* for(var j = 0; j < input[i].length; j++){
            console.log("No " +input[i][j])
       } */
       console.log("Nomor ID :"+input[i][0]);
       console.log("Nama Lengkap :"+input[i][1]);
       console.log("TTL :"+input[i][2]+input[i][3]);
       console.log("Hobi :"+input[i][4]+'\n');
    }
}
console.log("================================")
console.log("Jawaban Soal 4 Data Handling ");
console.log(dataHandling());


function balikKata(str) {

    var currentString = str; // simpan parametr str ke dalam variabel
    var newString = ''; // inisialisasi variabel 
    
    for (let i = str.length - 1; i >= 0; i--) { // lakukan looping
     newString = newString + currentString[i];
    }
   
    return newString;
}
console.log("================================")
console.log("Jawaban Soal 5 Balik Kata ");
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


var inputArr = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/1/1989", "Membaca"];

    
function dataHandling2(strArr){
    var resultArr = [];
    strArr.splice(1,1,"Roman Alamsyah Elsharawy");
    strArr.splice(2,1,"Provinsi Bandar Lampung");
    strArr.splice(4,1,"Pria");
    strArr.splice(5,1,"SMA Internasional Metro");
    console.log(strArr);
   
    var spltMonth = strArr[3].split("/");
    var getMonth  = spltMonth[1];
    switch(spltMonth[1]){
        case 1 :{console.log(" Januari ");break;}
        case 2 :{console.log(" Februari ");break;}
        case 3 :{console.log(" Maret ");break;}
        case 4 :{console.log(" April ");break;}
        case 5 :{console.log("Mei ");break;}
        case 6 :{console.log(" Juni ");break;}
        case 7 :{console.log(" Juli ");break;}
        case 8 :{console.log(" Agustus ");break;}
        case 9 :{console.log(" September ");break;}
        case 10 :{console.log("Oktober ");break;}
        case 11 :{console.log("November ");break;}
        case 12 :{console.log("Desember ");break;}
        default:{console.log("inputan tidak sesuai")}
    }
    return strArr;
}
console.log("==========================");
console.log("Jawaban Soal no 6 Handling data 2");
dataHandling2(inputArr)

