// Driver Code
var people = [ ["Bruce", "Banner", "male"], ["Natasha", "Romanoff", "female",1975] ]
var personObj = {
    firstName : "John",
    lastName: "Doe",
    gender: "male",
    age: 27
} 

function arrayToObject(people) {
    // Code di sini 
    var tmpObjs ={};
    var tmpCnt = 1;
    for(let i = 0; i < people.length; i++) {        
        var nama = tmpCnt+". "+people[i][0]+ " " +people[i][1]; 
        var now = new Date()
        var thisYear = now.getFullYear() ;
                
        tmpObjs[nama] = {
            'firstName' : people[i][0],
            'lastName' : people[i][1],
            'gender'   : people[i][2],
            'age'      : (people[i][3]) == null ? 'Invalid Birth Year' :  thisYear - people[i][3] 
        }
       
        tmpCnt++;
    }
    return tmpObjs;

}

console.log("Jawaban Soal No 1 Object");
console.log(arrayToObject(people));
console.log("==========================")
console.log("Jawaban Soal No 2 Object");
function shoppingTime(memberId, money) {
    
    if(memberId == ''){
        return 'Mohon maaf, toko x hanya berlaku untuk member saja'; 
    }else{
        var listPurchase = [
            ['Sepatu brand Stacattu', 1500000],
            ['Baju brand Zoro', 500000],
            ['Baju brand H&N', 250000], 
            ['Sweater brand Uniklooh', 175000], 
            ['Casing Handphone', 50000], 
        ];
        if(money < 50000 ){
            return 'Mohon maaf, uang tidak cukup';
        }else{
           
            var tmpPurchase = [];
            var obj = {};
            
            for(let i = 0; i < listPurchase.length; i++){
               var hrg = listPurchase[i][1];
               if(money >= hrg){
                 tmpPurchase.push(listPurchase[i][0])
               }
                
            }
            obj = {
                memberId : memberId,
                money : money,
                listPurchased :tmpPurchase
            };
            return obj;
        }


    }
}

console.log(shoppingTime('Xeri', 170000));
   
console.log("===============================");
console.log("Jawaban Soal no 3 Naik Angkot");


function naikAngkot(arrPenumpang) {
    
    var rutes = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
        var obj = {};
        var objTemp = {};
        for(var penumpang = 0; penumpang < arrPenumpang.length; penumpang++){
            
            let namaPenumpang = arrPenumpang[penumpang][0];
            var startRute = arrPenumpang[penumpang][1];
            var endRute = arrPenumpang[penumpang][2];
            var findStartIndex = rutes.indexOf(startRute);
            var findEndIndex = rutes.indexOf(endRute);
            var rangeRute = findEndIndex - findStartIndex;
            var hargaPerRute = 2000;
            var totHarga = hargaPerRute * rangeRute;
            obj = {
                penumpang :namaPenumpang,
                naikDari  :startRute,
                tujuan    :endRute,
                bayar     : totHarga
            }
            console.log(obj)
         }
    
}

naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]);
