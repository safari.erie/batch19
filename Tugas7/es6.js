
console.log("Jawaban no 1 Merubah menjadi arrow function");
console.log("===========================================");
const golden = () =>{
    console.log("this is golden!!")
}

golden();

console.log("===========================================");
console.log("Penyederhanan Object Literal");
console.log("===========================================");
const newFunction = (firstName, lastName) => {
   
    let objPerson = {
      lastName: lastName,
      lastName:lastName,
      fullName: function(){
        return `First Name ${firstName} dan Last Name ${lastName}`
      }
    }

    return objPerson;
  }
   
  //Driver Code 
  console.log("Jawaban Soal No 2 Sederhana")
  console.log(newFunction("William", "Imoh").fullName());


  console.log("======================");
  console.log("Jawaban No 3 Destruct ");
  console.log("======================");

  const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

  let {firstName, lastName, destination, occupation,spell } = newObject;

  console.log(firstName, lastName, destination, occupation,spell);


  console.log("======================");
  console.log("Jawaban No 4 Spred ");
  console.log("======================");
  const west = ["Will", "Chris", "Sam", "Holly"];
  const east = ["Gill", "Brian", "Noel", "Maggie"];
  spreadCombine = (str1, str2, str3, str4) =>{
    return str1 + str2 + str3 + str4
  }

 let comnninedArray = [...east, ...east]
 console.log(comnninedArray);


 console.log("======================");
 console.log("Jawaban No 5 Literal String ");
 console.log("======================");
 const planet = "earth"
 const view = "glass"


  const setLiteralString = (view, planet) =>{
      let resString = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;
    return resString;
  } 
  
  console.log(setLiteralString(view, planet))
 
