var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
];


let timeDefault = 10000;
let incrementBooks = 0;

callReadBooks = () =>{
    readBooks(timeDefault, books[incrementBooks],function(waktuCalback){
        timeDefault = waktuCalback;
        console.log(timeDefault);
        incrementBooks++;
        if(books[incrementBooks]) // check is of length array books, 
            callReadBooks();
    });
}

console.log("Jawaban Soal No 1 Callback")
console.log(callReadBooks());

/* function periksaDokter(nomerAntri, callback) {
    console.log(callback)
    if(nomerAntri > 50 ) {
        callback(false)
    } else if(nomerAntri < 10) {
        callback(true)
    }    
}  */

/* periksaDokter(65, function(check) {
    if(check) {
        console.log("sebentar lagi giliran saya")
    } else {
        console.log("saya jalan-jalan dulu")
    }
})  */

