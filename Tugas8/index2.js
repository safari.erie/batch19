var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]




let callPromise = async () => {
    let timeDefault = 10000;
    // loop array books
    
    for(let iBook = 0; iBook < books.length; iBook++)
    {
        timeDefault = await readBooksPromise(timeDefault, books[iBook]).then(function(waktu){
           return waktu;
        }).catch(function(waktu){
            return waktu;
        });
       
    }
}

console.log("Jawaban Soal No 2 Promise")
callPromise();
