
class Animal {
    
    constructor(name){
        this.name = name;
        this.clegs = 4;
        this.ccold_blooded  = false;
    }

    get cname(){
        return this.name;
    }

    get legs(){
        return this.clegs;
    }

    set legs(leg){
        this.clegs = leg;
    }

    get cold_blooded(){
        return this.ccold_blooded;
    }

    set cold_blooded(cold){
        this.ccold_blooded = cold;
    }

}


class Ape extends Animal{

    constructor(animal, paramApe){
        super(animal);
        this.paramApe = paramApe;
    }

    yell(){
        console.log("Auooo");
    }
}

class Frog extends Animal{


    constructor(animal, ParamJump){
        super(animal);
        this.paramJump = ParamJump;
    }

    Jump(){
        console.log('hop hop');
    }
}

console.log("Jawaban No 1");
animal = new Animal('Eri');
console.log(animal.cname);
console.log(animal.legs);
console.log(animal.cold_blooded);




console.log("Jawaban No 2");
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk");
kodok.Jump();